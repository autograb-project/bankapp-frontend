import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import userReducer from "../redux/user";
import { configureStore } from "@reduxjs/toolkit";

// let store = configureStore({
//   reducer: { userReducer },
// });

export function renderWithProviders(
  ui,
  {
    // Automatically create a store instance if no store was passed in
    store = configureStore({
      reducer: { userReducer },
    }),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>;
  }
  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
