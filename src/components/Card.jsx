import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

const CardComponent = ({
  title,
  total,
  text,
  link1,
  link2,
  link1Url,
  link2Url,
  param,
}) => {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2  fw-bold bg-danger text-white p-1">
          {total}
        </Card.Subtitle>
        <Card.Text>{text}</Card.Text>
        <Card.Link href={link1Url}>
          <Link style={{ textDecoration: "none" }} to={link1Url} state={param}>
            {link1}
          </Link>
        </Card.Link>
        <Card.Link>
          <Link style={{ textDecoration: "none" }} to={link2Url}>
            {link2}
          </Link>
        </Card.Link>
      </Card.Body>
    </Card>
  );
};

export default CardComponent;
