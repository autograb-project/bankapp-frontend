import { useState } from "react";
import {
  Button,
  Col,
  Container,
  Dropdown,
  Form,
  FormControl,
  FormGroup,
  FormLabel,
  Row,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import { createNewTransaction } from "../core/api/index";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Transaction = () => {
  const accountList = useSelector((state) => state.accountReducer.accountList);
  const loggedUser = useSelector((state) => state.authReducer.loggedUser);

  const [fromAcc, setFromAcc] = useState();
  const [toAcc, setToAcc] = useState();
  const [amount, setAmount] = useState();
  const [desc, setDesc] = useState();
  const [userAccs] = useState(
    accountList.filter((a) => loggedUser._id === a.user)
  );
  const navigate = useNavigate();
  const onSubmit = async (e) => {
    e.preventDefault();
    const data = {
      description: desc,
      amount: amount,
      transFromAccId: fromAcc,
      transToAccId: toAcc,
    };
    await axios
      .post("/transactions", data, {
        withCredentials: true,
        baseURL: "http://localhost:3001/api/v1",
      })
      // await createNewTransaction(data)
      .then((res) => {
        console.log(res);
        navigate("/dashboard");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <Container>
      <Col>
        <Row>
          <Col className="m-5">
            <h3>Transfer Money to Another Account</h3>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Form onSubmit={onSubmit} aria-label="form">
              <FormGroup className="mb-3 " controlId="from">
                {/* <FormLabel>Deposite Amount</FormLabel> */}
                <FormLabel data-testid="fromAccount">From Account</FormLabel>
                <Dropdown
                  data-testid="accounts"
                  onSelect={function (evt) {
                    setFromAcc(evt);
                  }}
                >
                  <Dropdown.Toggle variant="dark" id="dropdown-basic">
                    {fromAcc ? fromAcc : "Select transfering account"}
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    {userAccs?.map((it, index) => (
                      <Dropdown.Item
                        key={index}
                        eventKey={it._id}
                        value={fromAcc}
                        onSelect={(e) => {
                          setFromAcc(it._id);
                        }}
                        // onSelect={(value) => setFromAcc(value)}
                      >
                        {it.name} - $ {it.balance}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </FormGroup>
              <FormGroup className="mb-3 " controlId="to">
                <FormLabel data-testid="toAccount">To Account</FormLabel>
                <FormControl
                  type="text"
                  onChange={(e) => {
                    setToAcc(e.target.value);
                  }}
                  placeholder="ex: 65fc929922caeeaaa312d9ec"
                  value={toAcc}
                  aria-label="toAccount"
                />
              </FormGroup>
              <FormGroup className="mb-3" controlId="desc">
                <FormLabel data-testid="description">Description</FormLabel>
                <Form.Control
                  type="text"
                  onChange={(e) => {
                    setDesc(e.target.value);
                  }}
                  value={desc}
                  placeholder="Ex : Bill payments"
                  aria-label="descriptionTxt"
                />
              </FormGroup>
              <FormGroup className="mb-3" controlId="amount">
                <FormLabel data-testid="amount">Amount</FormLabel>
                <Form.Control
                  type="text"
                  onChange={(e) => {
                    setAmount(e.target.value);
                  }}
                  value={amount}
                  placeholder="Amount"
                  aria-label="amountTxt"
                />
              </FormGroup>

              <Button variant="primary" type="submit">
                Transfer
              </Button>
            </Form>
          </Col>
        </Row>
      </Col>
    </Container>
  );
};
export default Transaction;
