import { useEffect, useState } from "react";
import { Col, Container, Row, Table } from "react-bootstrap";
import { geAllTransactions } from "../core/api/index";

const TransactionList = () => {
  const [transAc, setTransac] = useState();
  useEffect(() => {
    getTransactions();
  }, []);

  const getTransactions = async () => {
    await geAllTransactions()
      .then((res) => setTransac(res.data.data.data))
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Container>
      <Col>
        <Row>
          <Col className="m-5">
            <h3>All Transactions</h3>
          </Col>
        </Row>
        <Row>
          <Col>
            {" "}
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Description</th>
                  <th>Amount</th>
                  <th>From Acc</th>
                  <th>To Acc</th>
                </tr>
              </thead>
              <tbody>
                {transAc?.map((it, index) => (
                  <tr key={index}>
                    <td>{index}</td>
                    <td>{it.description}</td>
                    <td>{it.amount}</td>
                    <td>{it.transFromAccId}</td>
                    <td>{it.transToAccId}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Col>
    </Container>
  );
};
export default TransactionList;
