import { Button } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../redux/authentication";
import { Link } from "react-router-dom";

function Nav() {
  const isLoggedIn = useSelector((state) => state.authReducer.isLoggedIn);
  const loggedUser = useSelector((state) => state.authReducer.loggedUser);

  let navigate = useNavigate();
  const dispatch = useDispatch();

  return (
    <Navbar className="bg-dark">
      <Container>
        <Navbar.Brand
        // href=
        // className="text-white"
        >
          <Link
            style={{ textDecoration: "none", color: "white" }}
            to={isLoggedIn ? "/dashboard" : "/home"}
          >
            {" "}
            Bank App
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text className="text-white">
            {isLoggedIn ? (
              <>
                Signed in as:
                <a href="#login" className="text-white m-2">
                  {loggedUser.name}
                  {loggedUser.role ? <>( {loggedUser.role} )</> : ""}
                </a>
                <Button
                  className="m-1"
                  size={"sm"}
                  variant="light"
                  onClick={(e) => {
                    e.preventDefault();
                    navigate("/home");
                    dispatch(authActions.logout());
                  }}
                >
                  Logout
                </Button>
              </>
            ) : (
              <Button
                variant="light"
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/login");
                }}
              >
                Login
              </Button>
            )}
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Nav;
