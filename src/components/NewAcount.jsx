import { useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import {
  Button,
  Col,
  Container,
  Form,
  FormControl,
  FormGroup,
  FormLabel,
  Row,
} from "react-bootstrap";
// import { accountActions } from "../redux/account";
import axios from "axios";

const NewAccount = () => {
  const loggedUser = useSelector((state) => state.authReducer.loggedUser);
  const [accType, setAccType] = useState("");
  const [amount, setAmount] = useState("");
  // const dispatch = useDispatch();
  const navigate = useNavigate();

  const onSubmit = async (e) => {
    e.preventDefault();
    const newAcc = {
      name: accType,
      balance: Number(amount?.replace("$", "")),
      user: loggedUser._id,
    };

    await axios
      .post("/accounts", newAcc, {
        withCredentials: true,
        baseURL: "http://localhost:3001/api/v1",
      })
      // await createNewAccount(newAcc)
      .then((res) => {
        navigate("/dashboard");
      })
      .catch((err) => {});
  };

  return (
    <Container className=" ">
      <Col>
        <Row className="d-flex justify-content-center">
          <Col>
            <h3 className="p-5">Create New Account</h3>
          </Col>
        </Row>
        <Row>
          <Col md={6} className="m-5 ">
            <Form onSubmit={onSubmit} aria-label="form-dep">
              <FormGroup className="mb-3" controlId="email">
                <FormLabel>Account Type</FormLabel>
                <FormControl
                  type="text"
                  onChange={(e) => {
                    setAccType(e.target.value);
                  }}
                  placeholder="Ex: Savings, Salary etc..."
                  value={accType}
                  aria-label="accountType"
                />
              </FormGroup>
              <FormGroup className="mb-3" controlId="password">
                <FormLabel>Deposite Amount</FormLabel>
                <Form.Control
                  type="text"
                  onChange={(e) => {
                    setAmount(e.target.value);
                  }}
                  value={amount}
                  placeholder="$1000"
                  aria-label="depositAmount"
                />
              </FormGroup>

              <Button variant="primary" type="submit" data-test-id="withdraw">
                Create New Account
              </Button>
            </Form>
          </Col>
        </Row>
      </Col>
    </Container>
  );
};

export default NewAccount;
