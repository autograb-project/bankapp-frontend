import { Col, Container, Row } from "react-bootstrap";

import Navbar from "./Navbar";

const SiteLayout = (props) => {
  return (
    <Container>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
      <Row>
        <Col className=" " md={12}>
          <main>{props.children}</main>
        </Col>
      </Row>
    </Container>
  );
};

export default SiteLayout;
