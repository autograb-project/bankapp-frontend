import { Route, Routes } from "react-router-dom";
import "./App.css";

import SiteLayout from "./components/SiteLayout";
import Login from "./components/Login";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import { useSelector } from "react-redux";
import Admin from "./components/Admin";
import NewAccount from "./components/NewAcount";
import Accounts from "./components/Accounts";
import Transaction from "./components/Transaction";
import TransactionList from "./components/TransactionList";

function App() {
  const isLoggedIn = useSelector((state) => state.authReducer.isLoggedIn);

  return (
    <SiteLayout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
        {isLoggedIn && (
          <>
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/create-new-account" element={<NewAccount />} exact />
            <Route path="/check-accounts" element={<Accounts />} exact />
            <Route path="/transactions" element={<Transaction />} exact />
            <Route
              path="/all-transactions"
              element={<TransactionList />}
              exact
            />
          </>
        )}
      </Routes>
    </SiteLayout>
  );
}

export default App;
