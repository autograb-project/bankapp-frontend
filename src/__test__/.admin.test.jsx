import { render, screen } from "@testing-library/react";
import Admin from "../components/Admin";
import { renderWithProviders } from "../utils/test.utils";
import store from "../redux/store";
import { Provider } from "react-redux";

const ReduxWrapper = ({ children }) => (
  <Provider store={store}>{children}</Provider>
);

describe("Admin component", () => {
  // renderWithProviders(<Admin />);
  render(<Admin />, { wrapper: ReduxWrapper });
  it("Should render h3 heading with text", () => {
    expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
      /Total Bank Balance/i
    );
  });
});
