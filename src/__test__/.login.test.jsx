import { render, screen, fireEvent } from "@testing-library/react";
import Login from "../components/Login";
import store from "../redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { act } from "react-dom/test-utils";

const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Login />
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );
  return component;
};
describe("Login component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render h3 heading with text", () => {
    expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
      /Login/i
    );
  });
  it("Should render a login form", () => {
    expect(screen.getByRole("form")).toBeInTheDocument();
  });
  it("Should render Email label", () => {
    expect(screen.getByLabelText("Email")).toBeInTheDocument();
  });
  it("Should render Email text field", () => {
    expect(screen.getByRole("textbox", { name: "email" })).toBeInTheDocument();
  });
  it("Should render Password label", () => {
    expect(screen.getByLabelText("Password")).toBeInTheDocument();
  });

  it("Should render Login button", () => {
    expect(screen.getByRole("button", { name: "Login" })).toBeInTheDocument();
  });

  it("test if Login form works", async () => {
    await act(async () => {
      const email = screen.getByRole("textbox", { name: "email" });
      expect(email).toHaveValue("");
      fireEvent.change(email, { target: { value: "John@gmail.com" } });
      expect(email).toHaveValue("John@gmail.com");
      const password = screen.getByPlaceholderText("password");
      fireEvent.change(password, { target: { value: "123" } });
      expect(password).toHaveValue("123");
      const button = screen.getByRole("button", { name: "Login" });
      fireEvent.click(button);
    });
    expect(window.location.pathname).toBe("/dashboard");
  });
});
