import { render, screen, fireEvent } from "@testing-library/react";
import Transaction from "../components/Transaction";
import store from "../redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { act } from "react-dom/test-utils";

import axios from "axios";

jest.mock("axios");
jest.mock("../core/api/index");
const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Transaction />
      <Routes>
        <Route path="/dashboard">Dashboard</Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("New Transaction component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render h3 heading with text", () => {
    expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
      /Transfer Money to Another Account/i
    );
  });
  it("Should render a new transfer form", () => {
    expect(screen.getByRole("form")).toBeInTheDocument();
  });
  it("Should render Account from label", () => {
    expect(screen.getByTestId("fromAccount")).toBeInTheDocument();
  });
  it("Should render Account Type  text field", () => {
    expect(screen.getByTestId("accounts")).toBeInTheDocument();
  });
  it("Should render To Account  label", () => {
    expect(screen.getByTestId("toAccount")).toBeInTheDocument();
  });
  it("Should render To Account text field", () => {
    expect(
      screen.getByRole("textbox", { name: "toAccount" })
    ).toBeInTheDocument();
  });
  it("Should render To Account  label", () => {
    expect(screen.getByTestId("description")).toBeInTheDocument();
  });
  it("Should render To Account text field", () => {
    expect(
      screen.getByRole("textbox", { name: "descriptionTxt" })
    ).toBeInTheDocument();
  });
  it("Should render To Account  label", () => {
    expect(screen.getByTestId("amount")).toBeInTheDocument();
  });
  it("Should render To Account text field", () => {
    expect(
      screen.getByRole("textbox", { name: "amountTxt" })
    ).toBeInTheDocument();
  });
  it("Should render Transfer  button", () => {
    expect(
      screen.getByRole("button", { name: "Transfer" })
    ).toBeInTheDocument();
  });

  //***********interaction testing

  it("Should render Transfer", async () => {
    axios.post = jest.fn().mockResolvedValue({
      data: [
        {
          userId: 1,
          id: 1,
          title: "test",
        },
      ],
    });
    await act(async () => {
      const accountFrom = screen.getByTestId("accounts");

      // fireEvent.change(accountFrom, { target: { value: "1" } });
      const toAccount = screen.getByRole("textbox", { name: "toAccount" });
      fireEvent.change(toAccount, { target: { value: "78798" } });
      expect(toAccount).toHaveValue("78798");
      const desc = screen.getByRole("textbox", { name: "descriptionTxt" });
      fireEvent.change(desc, { target: { value: "First Transfer" } });
      expect(desc).toHaveValue("First Transfer");
      const amount = screen.getByRole("textbox", { name: "amountTxt" });
      fireEvent.change(amount, { target: { value: "500" } });
      expect(amount).toHaveValue("500");

      const button = screen.getByRole("button", { name: "Transfer" });
      fireEvent.click(button);
    });

    expect(axios.post).toHaveBeenCalledTimes(1);

    expect(window.location.pathname).toBe("/dashboard");
  });
});
