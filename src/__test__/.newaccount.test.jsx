import { render, screen, fireEvent } from "@testing-library/react";
import NewAcount from "../components/NewAcount";
import store from "../redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";

jest.mock("axios");
jest.mock("../core/api/index");
const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <NewAcount />
      <Routes>
        <Route path="/dashboard">Dashboard</Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("New Bank Account component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render h3 heading with text", () => {
    expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
      /Create New Account/i
    );
  });
  it("Should render a new account form", () => {
    expect(screen.getByRole("form")).toBeInTheDocument();
  });
  it("Should render Account Type label", () => {
    expect(screen.getByLabelText("Account Type")).toBeInTheDocument();
  });
  it("Should render Account Type  text field", () => {
    expect(
      screen.getByRole("textbox", { name: "accountType" })
    ).toBeInTheDocument();
  });
  it("Should render Deposit Amount label", () => {
    expect(screen.getByLabelText("Deposite Amount")).toBeInTheDocument();
  });
  it("Should render Deposit Amount text field", () => {
    expect(
      screen.getByRole("textbox", { name: "depositAmount" })
    ).toBeInTheDocument();
  });
  it("Should render Create Account button", () => {
    expect(
      screen.getByRole("button", { name: "Create New Account" })
    ).toBeInTheDocument();
  });

  //***********interaction testing

  it("Should render Create Account button", async () => {
    axios.post = jest.fn().mockResolvedValue({
      data: [
        {
          userId: 1,
          id: 1,
          title: "test",
        },
      ],
    });
    await act(async () => {
      const accountType = screen.getByRole("textbox", { name: "accountType" });
      expect(accountType).toHaveValue("");
      fireEvent.change(accountType, { target: { value: "Savings" } });
      expect(accountType).toHaveValue("Savings");
      const depositAmount = screen.getByRole("textbox", {
        name: "depositAmount",
      });
      fireEvent.change(depositAmount, { target: { value: "500" } });
      expect(depositAmount).toHaveValue("500");
      const button = screen.getByRole("button", { name: "Create New Account" });
      fireEvent.click(button);
    });
    // const title = await createNewAccount({
    //   userId: 1,
    //   id: 1,
    //   title: "test",
    // });
    expect(axios.post).toHaveBeenCalledTimes(1);
    // expect(title).toEqual("test");

    // axios.get("https://www.google.com");
    expect(window.location.pathname).toBe("/dashboard");
  });
});
