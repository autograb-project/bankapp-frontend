import { createSlice } from "@reduxjs/toolkit";

const initialState = { accountsTotal: 0, accountList: [] };

const accountSlice = createSlice({
  name: "Accounts",
  initialState: initialState,
  reducers: {
    accountTotal(state, action) {
      state.accountsTotal = action.payload;
    },
    accountList(state, action) {
      state.accountList = action.payload;
    },
  },
});

export const accountActions = accountSlice.actions;
export default accountSlice.reducer;
