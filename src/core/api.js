import axios from "axios";
const api = axios.create({
  withCredentials: true,
  baseURL: "http://localhost:3001/api/v1",
});

export { api };
