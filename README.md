# Bank App React frontend project

(Design and development)

# Implemented and tested Scenarios

1. Create New bank account
2. Deposite to Account
3. Withdraw from Account
4. Transfer to another account
5. Check account Balances
6. Manager, check total bank balance
   7.List down Transactions
7. Hard coded Login

# Implemented and tested Scenarios

ReactJs
scss
redux
testing with jest

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm run test`

Test cases added for interactions.

### Test this project with folowing logins

Login hardcoded for following users

User 1 1. email: "Surya@gmail.com",
password: "123",

User 2 2. email: "John@gmail.com",
password: "123",,

Manager Role
email: "smith@gmail.com",
password: "123",
role: "Manager",

### Used techs

SCSS -Styling
Bootstrap/React-Bootstrap
Redux- state management
Jest- Testing
Axios- Api
